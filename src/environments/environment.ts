// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB80Bm47x4RPu2l2z_TxIid34V2yhS-9TU',
    authDomain: 'weddings-1192f.firebaseapp.com',
    databaseURL: 'https://weddings-1192f.firebaseio.com',
    projectId: 'weddings-1192f',
    storageBucket: 'weddings-1192f.appspot.com',
    messagingSenderId: '452731203859'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

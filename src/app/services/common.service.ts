import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AppConstant} from './../app.constant';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private _notification: BehaviorSubject<string> = new BehaviorSubject(null);
  readonly notification$: any = this._notification.asObservable();
  private _getFirebaseMessage: BehaviorSubject<any> = new BehaviorSubject(null);
  readonly getFirebaseMessage$: any = this._getFirebaseMessage.asObservable();
  constructor(private formBuilder: FormBuilder) { }
  createSignupForm() {
    return this.formBuilder.group({
      [AppConstant.signupForm.createdby]: [null, Validators.required],
      [AppConstant.signupForm.fullname]: [null, Validators.required],
      [AppConstant.signupForm.maritialstatus]: [null, Validators.required],
      [AppConstant.signupForm.gender]: [null, Validators.required],
      [AppConstant.signupForm.email]: [null, Validators.required],
      [AppConstant.signupForm.password]: [null, Validators.required],
      [AppConstant.signupForm.dob]: [null, Validators.required],
      [AppConstant.signupForm.caste]: [null, Validators.required],
      [AppConstant.signupForm.phone]: [null, Validators.required],
      // 'dob': [{value: null, disabled: true}, Validators.required],
      // 'country': [null, Validators.required],
      // 'state': [null, Validators.required],
      // 'city': [null, Validators.required],
      [AppConstant.signupForm.religion]: [null, Validators.required],
      [AppConstant.signupForm.mothertongue]:  [null, Validators.required],
      // 'caste': [null, Validators.required],
      // 'email': [null, Validators.required],
      // 'mobile': [null, Validators.required],
      // 'password':[null, Validators.required]
    });
  }
  createSearchForm() {
    return this.formBuilder.group({
      [AppConstant.searchForm.fromage]: [null],
      [AppConstant.searchForm.toage]: [null],
      [AppConstant.searchForm.createdby]: [null],
      [AppConstant.searchForm.maritialstatus]: [null],
      [AppConstant.searchForm.caste]: [null],
      // 'dob': [{value: null, disabled: true}, Validators.required],
      // 'country': [null, Validators.required],
      // 'state': [null, Validators.required],
      // 'city': [null, Validators.required],
      [AppConstant.searchForm.religion]: [null],
      [AppConstant.searchForm.mothertongue]:  [null],
      // 'caste': [null, Validators.required],
      // 'email': [null, Validators.required],
      // 'mobile': [null, Validators.required],
      // 'password':[null, Validators.required]
    });
  }
  createPartnerPreferenceForm() {
    return this.formBuilder.group({
      [AppConstant.partnerPreferenceForm.fromage]: [null],
      [AppConstant.partnerPreferenceForm.toage]: [null],
      [AppConstant.partnerPreferenceForm.createdby]: [null],
      [AppConstant.partnerPreferenceForm.maritialstatus]: [null],
      [AppConstant.partnerPreferenceForm.caste]: [null],
      // 'dob': [{value: null, disabled: true}, Validators.required],
      // 'country': [null, Validators.required],
      // 'state': [null, Validators.required],
      // 'city': [null, Validators.required],
      [AppConstant.partnerPreferenceForm.religion]: [null],
      [AppConstant.partnerPreferenceForm.mothertongue]:  [null],
      [AppConstant.partnerPreferenceForm.education]:  [null],
      [AppConstant.partnerPreferenceForm.gon]:  [null],
      [AppConstant.partnerPreferenceForm.industry]:  [null],
      [AppConstant.partnerPreferenceForm.income]:  [null],
      [AppConstant.partnerPreferenceForm.familyincome]:  [null],
      [AppConstant.partnerPreferenceForm.bodytype]:  [null],
      [AppConstant.partnerPreferenceForm.familytype]:  [null],
      [AppConstant.partnerPreferenceForm.smoking]:  [null],
      [AppConstant.partnerPreferenceForm.fromheight]:  [null],
      [AppConstant.partnerPreferenceForm.toheight]:  [null],
      [AppConstant.partnerPreferenceForm.drinking]:  [null],
      [AppConstant.partnerPreferenceForm.gothra]:  [null],
      [AppConstant.partnerPreferenceForm.foodhabits]:  [null],
      [AppConstant.partnerPreferenceForm.occupation]:  [null],
      [AppConstant.partnerPreferenceForm.manglik]:  [null],
      // 'caste': [null, Validators.required],
      // 'email': [null, Validators.required],
      // 'mobile': [null, Validators.required],
      // 'password':[null, Validators.required]
    });
  }
  createLoginForm() {
    return this.formBuilder.group({
      [AppConstant.loginForm.email]: [null, Validators.required],
      [AppConstant.loginForm.password]: [null, Validators.required]
    });
  }

  notify(message) {
    this._notification.next(message);
    setTimeout(() => this._notification.next(null), 1000);
  }
  getFirebaseMessage(data) {
    this._getFirebaseMessage.next(data);
  }
  calculate_age(dob) {
    const dateDifferent = Date.now() - dob.getTime();
    const ageDifference = new Date(dateDifferent);
    return Math.abs(ageDifference.getUTCFullYear() - 1970);
  }
}

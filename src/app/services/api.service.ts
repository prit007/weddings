import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

// import 'rxjs/add/operator/catch';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';

import { LoadingBarService } from '@ngx-loading-bar/core';
import {CommonService} from './common.service';
import { first } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public afDb: AngularFireDatabase, private afs: AngularFirestore,
              private afAuth: AngularFireAuth, private loadingBar: LoadingBarService,
              private router: Router, private httpClient: HttpClient, private commonService: CommonService) { }
    addFireStore(collectionPath: string, data: any, documentPath?: string): any {
      if (documentPath) {
        return this.afs.collection(collectionPath).doc(documentPath).set(data);
      } else {
        return this.afs.collection(collectionPath).add(data);
      }
    }
    getFireStoreList(collectionPath, documentPath?): any {
      this.loadingBar.start();
      return new Promise((resolve, reject): any => {
      const getObservable: any = documentPath ? this.afs.collection(collectionPath).doc(documentPath).valueChanges()
                                : this.afs.collection(collectionPath).valueChanges();
      return getObservable.subscribe(res => { this.commonService.getFirebaseMessage(res); this.loadingBar.complete(); resolve(res); },
             (err) => { this.commonService.getFirebaseMessage(err); this.loadingBar.complete(); reject(err); });
      });
     }
     getFireStoreListDataOnParam(collectionPath, paramList, operator?): any {
      this.loadingBar.start();
      let combinedWhereClause: any;
      let getOperator: any;
      if (operator) {
          getOperator = operator;
      }
      return new Promise((resolve, reject): any => {
      const getObservable: any = this.afs.collection(collectionPath, ref => {
            paramList.map((data, index) => {
              if (data.operator) {
                  getOperator = data.operator;
                  delete data.operator;
              }
              if (Array.isArray(Object.values(data)[0])) {
                  data[Object.keys(data)[0]].map((subValue, subIndex) => {
                    if (!combinedWhereClause) {
                      combinedWhereClause = ref.where(Object.keys(data)[0], getOperator, subValue);
                   } else {
                      combinedWhereClause = combinedWhereClause.where(Object.keys(data)[0], getOperator, subValue);
                   }
                  });

              } else {
               if (!combinedWhereClause) {
                  combinedWhereClause = ref.where(Object.keys(data)[0], getOperator, Object.values(data)[0]);
               } else {
                 combinedWhereClause = combinedWhereClause.where(Object.keys(data)[0], getOperator, Object.values(data)[0]);
               }
              }
            });
            return combinedWhereClause; })
            .valueChanges();
      return getObservable.subscribe(res => { this.commonService.getFirebaseMessage(res); this.loadingBar.complete(); resolve(res); },
             (err) => { this.commonService.getFirebaseMessage(err); this.loadingBar.complete(); reject(err); });
      });
     }
    showError(err) {
      console.log('show error');
      alert('error');
    }
    signInEmailAndPassword(data): any {
      this.loadingBar.start();
      //let promise: any = this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password)       
      return new Promise((resolve, reject): any => {
      let promise: any = this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password);        
       return promise.then(res => {this.commonService.getFirebaseMessage(res);  resolve(res)})
        .catch(err => {this.commonService.getFirebaseMessage(err); reject(err)})
        .finally(data => {this.loadingBar.complete();})
      });
      // let promise: any = this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password);
      // return this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password).catch(err => {this.showError(err); return Observable.throw(err)}).finally(data => {alert('complete'); this.loadingBar.complete();})
    }
  
    signupWithEmailPassword(data): any{
      this.loadingBar.start();
      //let promise: any = this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password)       
      return new Promise((resolve, reject): any => {
        let promise: any = this.afAuth.auth.createUserWithEmailAndPassword(data.email, data.password);        
       return promise.then(res => { this.commonService.getFirebaseMessage(res); resolve(res) })
        .catch(err => { this.commonService.getFirebaseMessage(err); reject(err) })
        .finally(data => { this.loadingBar.complete();})
      });
    }
    httpGet(url: string, options?): Observable<ArrayBuffer> {
      return this.httpClient.get(url, options);
    }
    logout() {
      this.afAuth.auth.signOut();
    }
    isLoggedIn() {
      return this.afAuth.authState.pipe(first()).toPromise();
    }
}

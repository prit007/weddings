import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { AngularFireAuth } from '@angular/fire/auth';
import { Router, Event, NavigationError } from '@angular/router';
import {ApiService} from '../services/api.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private afAuth: AngularFireAuth, private router: Router, private apiService: ApiService) {}
  async doSomething() {
    const user = await this.apiService.isLoggedIn();
    if (user) {
      //this.router.navigate(['/']);
      return true;
    } else {
      this.router.navigate(['/home']);
      return false;
   }
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     return this.doSomething();
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import {SharedModule} from './modules/shared/shared.module';
import { AngularFireModule } from '@angular/fire';
import { NgxsModule } from '@ngxs/store';
import { TutorialState } from './state/tutorial.state';

import {CommonService} from './services/common.service';
import {ApiService} from './services/api.service';
import { HomeComponent } from './modules/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,

    LoadingBarRouterModule,
    LoadingBarModule,
    AngularFireModule.initializeApp(environment.firebase),
    SharedModule,   
    NgxsModule.forRoot([
      TutorialState
    ])
  ],
  providers: [CommonService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

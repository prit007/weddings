import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortlistRoutingModule } from './shortlist-routing.module';
import { ShortlistComponent } from './shortlist.component';

@NgModule({
  declarations: [ShortlistComponent],
  imports: [
    CommonModule,
    ShortlistRoutingModule
  ]
})
export class ShortlistModule { }

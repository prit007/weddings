import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ShortlistComponent } from './shortlist.component';

const routes: Routes = [
  {
    path: '',
    component: ShortlistComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShortlistRoutingModule { }

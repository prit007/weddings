import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInteractionsComponent } from './my-interactions.component';

describe('MyInteractionsComponent', () => {
  let component: MyInteractionsComponent;
  let fixture: ComponentFixture<MyInteractionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInteractionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInteractionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MyInteractionsComponent } from './my-interactions.component';

const routes: Routes = [
  {
    path: '',
    component: MyInteractionsComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyInteractionsRoutingModule { }

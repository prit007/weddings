import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyInteractionsRoutingModule } from './my-interactions-routing.module';
import { MyInteractionsComponent } from './my-interactions.component';

@NgModule({
  declarations: [MyInteractionsComponent],
  imports: [
    CommonModule,
    MyInteractionsRoutingModule
  ]
})
export class MyInteractionsModule { }

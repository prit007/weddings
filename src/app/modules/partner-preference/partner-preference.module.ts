import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartnerPreferenceComponent } from './partner-preference.component';
import { PartnerPreferenceRoutingModule } from './partner-preference-routing.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [PartnerPreferenceComponent],
  imports: [
    CommonModule,
    PartnerPreferenceRoutingModule,
    SharedModule
  ]
})
export class PartnerPreferenceModule { }

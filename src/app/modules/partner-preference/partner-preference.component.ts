import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {AppConstant} from '../../app.constant';
import { CommonService } from '../../services/common.service';
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  selector: 'app-partner-preference',
  templateUrl: './partner-preference.component.html',
  styleUrls: ['./partner-preference.component.scss']
})
export class PartnerPreferenceComponent implements OnInit {
  masterDataList: any;
  partnerPreferenceForm: FormGroup;
  uid: string;
  get f(): any { return this.partnerPreferenceForm.controls; }
  get appConstant(): any {return AppConstant};
  constructor(private apiService: ApiService, private commonService: CommonService,
              private afAuth: AngularFireAuth) { }

  ngOnInit() {
    this.getMasterData();
    this.createPartnerPreferenceForm();
    this.updatePartnerPrefereceForm();
  }
  getMasterData() {
    const allMasterData: object = {};
    this.apiService.getFireStoreList('/masterlist').then(data => {
      data.map((element, key) => {
        if (Object.keys(element).length > 0) {
          allMasterData[Object.keys(element)[0]] = Object.values(element)[0];
        }
      });
      this.masterDataList = allMasterData;
      console.log('check');
      console.log(this.masterDataList);
    }).catch(err => {
      console.log('--------------error');
      console.log(err);
    });
    this.getUserDetails();
  }
  updatePartnerPrefereceForm() {
    this.apiService.getFireStoreList('/partnerpreference', this.uid).then(data => {
      console.log('get partner preference data');
      console.log(data);
      const partnerPreferenceData: any = data;
      if (partnerPreferenceData && partnerPreferenceData.length && partnerPreferenceData.length === 1 &&
      (Object.keys(partnerPreferenceData[0]).length > 0 && partnerPreferenceData[0].constructor === Object)) {
        for (const key in partnerPreferenceData[0]) {
          if (key) {
            if (partnerPreferenceData[0][key].constructor === Object) {
              this.partnerPreferenceForm.get(key).patchValue(partnerPreferenceData[0][key].value);
            } else {
                this.partnerPreferenceForm.get(key).patchValue(partnerPreferenceData[0][key]);
            }
          }
        }
      }
    }).catch(err => {
      console.log('--------------error');
      console.log(err);
    });
  }
  getUserDetails() {
    this.afAuth.authState.subscribe(user => {
      if (user)  {
        this.uid = user.uid;
      }
    });
  }
  createPartnerPreferenceForm() {
    this.partnerPreferenceForm = this.commonService.createPartnerPreferenceForm();
  }
  onSubmit(data, valid) {
    const formData: any = {};
    for (let key in data) {
        if (data[key]) {
          formData[key] = data[key];
        }
    }
    this.apiService.addFireStore('/partnerpreference', formData, this.uid);
    console.log(formData)
  }
  // btnClick() {
  //   let educationString = "Manushya+++Deb+++Debari";
  //   let educationArray: any = educationString.split("+++");
  //   let educationObj: any = {gon: educationArray};
  //   console.log(educationObj);
  //   this.apiService.addFireStore('/masterlist', educationObj, '/gondoc')
  // }

}



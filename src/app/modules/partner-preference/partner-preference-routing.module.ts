import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PartnerPreferenceComponent } from './partner-preference.component';

const routes: Routes = [
  {
    path: '',
    component: PartnerPreferenceComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerPreferenceRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../services/common.service'
import { from } from 'rxjs';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  errorMessage: string;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.getFirebaseMessages();
  }
  getFirebaseMessages() {
    this.commonService.getFirebaseMessage$.subscribe(data => {
      console.log('get messages');
      console.log(data)
      if(data && data.message) {
        this.errorMessage = data.message;
        setTimeout(() => this.errorMessage = null, 5000)
      }
    })
  }

}

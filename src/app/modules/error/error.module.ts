import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ErrorRoutingModule} from './error-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorComponent } from './error.component';

import { ErrorsHandler } from './errors-handler';
import { ErrorsService } from './errors.service';
import { ServerErrorsInterceptor } from './server-errors.interceptor';

@NgModule({
  imports: [
    CommonModule,
    ErrorRoutingModule
  ],
  declarations: [ErrorComponent],
  providers: [
    ErrorsService,
    {
      provide: ErrorHandler,
      useClass: ErrorsHandler,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true
    },]
})
export class ErrorModule { }

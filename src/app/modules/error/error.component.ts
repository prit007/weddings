import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import {CommonService} from '../../services/common.service';
@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  routeParams;
  data;
  constructor(private activatedRoute: ActivatedRoute, private commonService: CommonService) { }
  ngOnInit() {
    this.routeParams = this.activatedRoute.snapshot.queryParams;
    this.data = this.activatedRoute.snapshot.data;
    this.getFirebaseMessage();
  }
  getFirebaseMessage() {
    this.commonService.getFirebaseMessage$.subscribe(data => {
      console.log('get message');
      console.log(data);
    });
  }

}

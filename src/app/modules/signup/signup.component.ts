import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CommonService} from '../../services/common.service';
import {ApiService} from '../../services/api.service';
import {AppConstant} from '../../app.constant';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { Router, Event, NavigationError } from '@angular/router';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SignupComponent implements OnInit {
  masterDataList: any;
  signupForm: FormGroup;
  formFields: any;
  endDate: any;
  isGenderChecked: boolean = true;
  constructor(private commonService: CommonService, private apiService: ApiService,
              private router: Router) { }
  get f(): any { return this.signupForm.controls; }
  get appConstant(): any {return AppConstant}

  ngOnInit() {
    this.formFields = AppConstant.signupForm;
    this.getMasterData();
    this.createSignupForm();
  }
  getMasterData() {
    let allMasterData:object = {};
    this.apiService.getFireStoreList('/masterlist').then(data => {
      data.map((element, key) => {
        if (Object.keys(element).length > 0) {
          allMasterData[Object.keys(element)[0]] = Object.values(element)[0]
        }
      });
      this.masterDataList = allMasterData;
      console.log('check');
      console.log(this.masterDataList);
    }).catch(err => {
      console.log('--------------error');
      console.log(err);
    });
  }
  checkProfileIdExist(formData) {
    const generateProfileId: string = 'NW' + Math.floor(Math.random() * 100000000);
    const profileIdDetails: Array<object> = [{profileId: generateProfileId}];
    this.apiService.getFireStoreListDataOnParam('/users', profileIdDetails, AppConstant.equalQueryOperator).then(data => {
      console.log('get the data');
      console.log(data);
      if (data.length === 0) {
          formData.profileId = generateProfileId;
          this.submitUserDetails(formData);
      } else {
           this.checkProfileIdExist(formData);
      }
    });
  }
  createSignupForm() {
    this.signupForm = this.commonService.createSignupForm();
    this.signupForm.controls[AppConstant.signupForm.dob].disable() ;
  }
  genderChange(data) {
    this.isGenderChecked = false;
    const currentDate =  new Date();
    if (data && data.value === 'male') {
    this.endDate = new Date(currentDate.setFullYear(new Date().getFullYear() - 21));
    } else if (data && data.value === 'female') {
    this.endDate = new Date(currentDate.setFullYear(new Date().getFullYear() - 18));
    }
    this.signupForm.controls[AppConstant.signupForm.dob].enable() ;
  }
  onSubmit(data, isValid) {
    console.log(data);
    data[AppConstant.age] = this.commonService.calculate_age(data.dob);
    console.log(isValid);
    if (isValid) {
      this.apiService.signupWithEmailPassword(data).then(res => {
        console.log('--------------res');
        console.log(res);
        this.checkProfileIdExist(data);
      }).catch(err => {
        console.log('--------------error');
        console.log(err);
      });
    }
  }
  submitUserDetails(data) {
    data.createdAt = Date.now();
    // this.apiService.getFireStoreList('users');
    this.apiService.addFireStore('users', data).then(res => {
      console.log('added');
      console.log(res);
      this.router.navigate(['/']);
    }).catch(err => {
      console.log(err);
    });
  }
}

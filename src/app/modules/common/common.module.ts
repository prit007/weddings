import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonRoutingModule } from './common-routing.module';
import {CommonComponent} from './common.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CommonRoutingModule,
    SharedModule
  ],
  declarations: [CommonComponent]
})
export class ComModule { }

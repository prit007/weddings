
export const navigation = [
{
  pageName: 'Partner Preference',
  pageUrl: '/partner-preference',
},
{
  pageName: 'My Matches',
  pageUrl: '/my-matches',
},
{
    pageName: 'Membership Plan',
    pageUrl: '/membership-plan',
},
{
    pageName: 'My Subscription',
    pageUrl: '/my-subscription',
},
{
    pageName: 'My Interactions',
    pageUrl: '/my-interactions',
},
{
    pageName: 'My Shortlist',
    pageUrl: '/my-shortlist',
},
{
    pageName: 'Rejected List',
    pageUrl: '/rejected-list',
},
{
    pageName: 'Settings',
    pageUrl: '/settings',
}
];

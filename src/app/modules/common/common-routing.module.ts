import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ModuleWithProviders} from '@angular/core';

import {CommonComponent} from './common.component';

const routes: Routes = [
  // { path: 'web', redirectTo: 'common', pathMatch: 'full'},
  // { path: 'common', component: CommonComponent },
  // { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  { path: '', component: CommonComponent ,
  children: [
    {
      path: 'dashboard',
      loadChildren: '../dashboard/dashboard.module#DashboardModule',
    },
    {
      path: 'search',
      loadChildren: '../search/search.module#SearchModule',
    },
    {
      path: 'partner-preference',
      loadChildren: '../partner-preference/partner-preference.module#PartnerPreferenceModule',
    },
    {
      path: 'my-matches',
      loadChildren: '../my-matches/my-matches.module#MyMatchesModule',
    },
    {
      path: 'my-interactions',
      loadChildren: '../my-interactions/my-interactions.module#MyInteractionsModule',
    },
    {
      path: 'my-shortlist',
      loadChildren: '../shortlist/shortlist.module#ShortlistModule',
    },
    {
      path: "error",
      loadChildren: '../error/error.module#ErrorModule'
    }
  ]},
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommonRoutingModule { }

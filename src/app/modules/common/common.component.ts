import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngxs/store';
import { AddTutorial } from '../.././actions/tutorial.actions';
import {MediaMatcher} from '@angular/cdk/layout';
import {ApiService} from '../../services/api.service';
import {CommonService} from '../../services/common.service';
import { Router, Event, NavigationError } from '@angular/router';
import { navigation } from './navigation';
@Component({
  selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.scss']
})
export class CommonComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  its: any = undefined;
  notification: string;
  showNotification: boolean;
  navLists: Array<object> = navigation;
  private _mobileQueryListener: () => void;
  fillerNav = Array(10).fill(0).map((_, i) => `Nav Item ${i + 1}`);
    fillerContent = Array(50).fill(0).map(() =>
      `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);
  constructor(private store: Store, changeDetectorRef:  
  ChangeDetectorRef, media: MediaMatcher, private apiService: ApiService, private commonService: CommonService,
  private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
   }
  
  addTutorial(name, url) {
      this.store.dispatch(new AddTutorial({name: name, url: url}))
  }
  
  ngOnInit() {
    this.commonService
    .notification$
    .subscribe(message => {
      this.notification = message;
      this.showNotification = true;
    });
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  fireClientError() {
    // throw new Error('Client Error. Shit happens :)');
    // it is not defined, ups
    return this.its.happens;
  }
  fireServerError() {
    this.apiService
            .httpGet  ('https://jsonplaceholder.typicode.com/1')
            .subscribe();
  }
  logout() {
    this.apiService.logout();
    this.router.navigate(['/home']);
  }
}

import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import {ApiService} from '../../services/api.service';
import { Router, Event, NavigationError } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    this.loginCheck();
  }

  async loginCheck() {
    const user = await this.apiService.isLoggedIn();
    if (user) {
      this.router.navigate(['/']);
    }
  }

}

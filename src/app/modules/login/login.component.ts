import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CommonService} from '../../services/common.service';
import {ApiService} from '../../services/api.service';
import {AppConstant} from '../../app.constant';
import { Router, Event, NavigationError } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private commonService: CommonService, private apiService: ApiService,
              private router: Router) { }
  get f(): any { return this.loginForm.controls; }
  get appConstant(): any { return AppConstant; }
  ngOnInit() {
    this.createLoginForm();
  }
  createLoginForm() {
    this.loginForm = this.commonService.createLoginForm();
  }
  onSubmit(data, isValid){
    console.log(data)
    this.apiService.signInEmailAndPassword(data).then(res => {
      console.log('------------------login');
      console.log(res);
      this.router.navigate(['/']);
    }).catch(err => {
      console.log('--------------login error');
      console.log(err);
    });
  }
}

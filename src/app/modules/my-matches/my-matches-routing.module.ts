import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MyMatchesComponent } from './my-matches.component';

const routes: Routes = [
  {
    path: '',
    component: MyMatchesComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyMatchesRoutingModule { }

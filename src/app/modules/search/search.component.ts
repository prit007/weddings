import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import {AppConstant} from '../../app.constant';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  masterDataList: any;
  searchForm: FormGroup;
  userData: any;
  get f(): any { return this.searchForm.controls; }
  get appConstant(): any {return AppConstant};
  constructor(private commonService: CommonService, private apiService: ApiService) { }

  ngOnInit() {
    this.getMasterData();
    this.createSearhForm();
  }
  clickPush() {
    this.apiService.addFireStore('/masterlist', {agelist : this.masterDataList.fromage}, 'agelistdoc').then(res => {
      console.log('list added');
    }).catch(err => {
      console.log(err);
    });
  }
  getMasterData() {
    const allMasterData: object = {};
    this.apiService.getFireStoreList('/masterlist').then(data => {
      data.map((element, key) => {
        if (Object.keys(element).length > 0) {
          allMasterData[Object.keys(element)[0]] = Object.values(element)[0];
        }
      });
      this.masterDataList = allMasterData;
      console.log('check');
      console.log(this.masterDataList);
    }).catch(err => {
      console.log('--------------error');
      console.log(err);
    });
  }
  createSearhForm() {
      this.searchForm = this.commonService.createSearchForm();
  }
  onSubmit(data, isValid) {
    console.log('searh form data');
    console.log(data);
    const searchObj: Array<object> = [];
    for (const key in data) {
        if (data[key]) {
          if (key === AppConstant.searchForm.fromage) {
            searchObj.push({age: data[key], operator: AppConstant.greaterThanEqualQueryOperator});
          } else if (key === AppConstant.searchForm.toage) {
            searchObj.push({age: data[key], operator: AppConstant.lessThanEqualQueryOperator});
          } else {
            searchObj.push({[key]: data[key], operator: AppConstant.equalQueryOperator});
          }
        }
    }
    console.log(searchObj);
    if (Object.keys(searchObj).length === 0) {
      this.apiService.getFireStoreList('/users').then(val => {
        console.log('get the search data');
        console.log(val);
        this.userData = val;
      });
    }  else {
      this.apiService.getFireStoreListDataOnParam('/users', searchObj).then(val => {
        console.log('get the search data');
        console.log(val);
        this.userData = val;
    });
    }
    // if (Object.keys(searchObj).length === 1 ) {
    //     if (searchObj.hasOwnProperty(AppConstant.searchForm.fromage) && searchObj[AppConstant.searchForm.fromage]) {
    //       this.apiService.getFireStoreListDataOnParam('/users', [{age : searchObj[AppConstant.searchForm.fromage]}],
    //       AppConstant.greaterThanEqualQueryOperator).then(val => {
    //         console.log('get the search data');
    //         console.log(val);
    //         this.userData = val;
    //       });
    //     } else if (searchObj.hasOwnProperty(AppConstant.searchForm.toage) && searchObj[AppConstant.searchForm.toage]) {
    //       this.apiService.getFireStoreListDataOnParam('/users', [{age : searchObj[AppConstant.searchForm.toage]}],
    //       AppConstant.lessThanEqualQueryOperator).then(val => {
    //         console.log('get the search data');
    //         console.log(val);
    //         this.userData = val;
    //       });
    //     }
    // }
    // if (Object.keys(searchObj).length === 2 ) {
    //   if ((searchObj.hasOwnProperty(AppConstant.searchForm.fromage) && searchObj[AppConstant.searchForm.fromage])
    //      && (searchObj.hasOwnProperty(AppConstant.searchForm.toage) && searchObj[AppConstant.searchForm.toage])) {
    //     this.apiService.getFireStoreListDataOnParam('/users', [{age : searchObj[AppConstant.searchForm.fromage]},
    //     {age : searchObj[AppConstant.searchForm.toage]}],
    //     AppConstant.greaterThanEqualQueryOperator).then(val => {
    //       console.log('get the search data');
    //       console.log(val);
    //       this.userData = val;
    //     });
    //   }
    // }
  }

}

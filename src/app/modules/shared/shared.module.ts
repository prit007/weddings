import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {AngularFireDatabaseModule} from '@angular/fire/database';

import { FlexLayoutModule } from '@angular/flex-layout';
// import { FlexLayoutServerModule } from '@angular/flex-layout/server';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatMenuModule, MatIconModule, MatBadgeModule,
        MatSidenavModule,  MatListModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatRadioModule,
        MatDatepickerModule, MatNativeDateModule, MatTabsModule, MatCardModule} from '@angular/material';
import { MessagesComponent } from '../messages/messages.component';
import {SignupComponent} from '../signup/signup.component';
import {CommonService} from '../../services/common.service';
import {ApiService} from '../../services/api.service';
import {AuthGuard} from '../../guards/auth.guard';
import { LoginComponent } from '../login/login.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoadingBarModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FlexLayoutModule,
    
    [MatButtonModule, MatCheckboxModule, MatToolbarModule, MatMenuModule, MatIconModule, MatBadgeModule,
    MatSidenavModule, MatListModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatRadioModule,
    MatDatepickerModule, MatNativeDateModule, MatTabsModule, MatCardModule]
  ],
  declarations: [SignupComponent, MessagesComponent, LoginComponent],
  providers: [CommonService, ApiService, AuthGuard, { provide: FirestoreSettingsToken, useValue: {} }],
  exports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoadingBarModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FlexLayoutModule,
    
    [MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatCardModule,
    SignupComponent,
    MessagesComponent,
    LoginComponent
    ]
  ]
})
export class SharedModule {}
